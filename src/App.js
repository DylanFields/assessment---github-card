import React from 'react';
import './App.css';
import { Button } from 'semantic-ui-react';

const GITHUB_USER_INFO = "https://api.github.com/users/davegregg";

class App extends React.Component {
  state = {
    user: {},
    active: false
  }

  handleToggle = () => {
    console.log("button was clicked!");
    if (this.state.active === true) {
      this.setState({active: false})
    } else {
    fetch(GITHUB_USER_INFO).then(response => response.json())
    .then(result =>  {
      console.log(result);
      this.setState({user: result, active: true});
    })
    }
  }
  render () {
  return (
  <React.Fragment>
     <Button onClick={this.handleToggle}>Toggle</Button>
      {this.state.active === true && (
      <React.Fragment> <img src={this.state.user.avatar_url} />
      <h2>{this.state.user.name}</h2>
      <p>{this.state.bio}</p>
      <p>Followers: {this.state.user.followers}</p>
      </React.Fragment>
      )}
  </React.Fragment>
  );
  }
}

export default App;
